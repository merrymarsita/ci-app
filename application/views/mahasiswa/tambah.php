<div class="container">

	<div class="row mt-3">
		<div class="col-md-6">

			<div class="card">
				<div class="card-header">
						Form Tambah Data Mahasiswa
				</div>
				<div class="card-body">
					
					<form action="" method="post">
						<div class="form-group">
							<label for="nama">Nama</label>
							<input type="text" name="nama" class="form-control" id="nama">
							<small class="form-text text-danger"><?php echo form_error('nama') ?></small>
						</div>
						<div class="form-group">
							<label for="nrp">NRP</label>
							<input type="text" name="nrp" class="form-control" id="nrp">
							<small class="form-text text-danger"><?php echo form_error('nrp') ?></small>
						</div>
						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" name="email" class="form-control" id="email">
							<small class="form-text text-danger"><?php echo form_error('email') ?></small>
						</div>
						<div class="form-group">
    						<label for="urusan">Jurusan</label>
    						<select class="form-control" id="jurusan" name="jurusan">
      						<option>Teknik Informatika</option>
      						<option>Teknik Mesin</option>
      						<option>Teknik Planologi</option>
     						<option>Teknik Lingkungan</option>
      						</select>
  						</div>
  						<button type="submit" name="tambah" class="btn btn-primary float-right">Tambah Data</button>
					</form>
			</div>	
		</div>
	</div>
</div>